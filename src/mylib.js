const mylib={

    /** simple add function*/
    add:(a,b)=>{return a+b},

    /** simple sub function*/
    sub:(a,b)=>{return a-b},
    
    /** simple mul function*/
    mul:(a,b)=>{return a*b},

    /** simple div function*/
    div:(a,b)=>{return a/b},

};

module.exports =mylib;