const expect =require('chai').expect;
const mylib = require('../src/mylib');
describe("Tests for mylib",()=>{

    it("add function",()=>{
        expect(mylib.add(1,2)).equal(3,"1+2!=3");
        expect(mylib.add(12340,21378)).equal(33718,"12340+21378!= 33718");
    });

    it("sub function",()=>{
        expect(mylib.sub(1,2)).equal(-1,"1-2!=-1");
        expect(mylib.sub(33718,9985)).equal(23733,"33718-9985!= 23733");
    });

    it("mul function",()=>{
        expect(mylib.mul(2,2)).equal(4,"2*2!=4");
        expect(mylib.mul(1234,1123)).equal(1385782,"1234*1123!= 1385782");
    });

    it("div function",()=>{
        expect(mylib.div(8,4)).equal(2,"8/4!=2");
        expect(mylib.div(12340,0)).equal(123,"the function cannot divide by zero");
    });

});
